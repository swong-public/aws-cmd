A simple cli interface to AWS using Boto

Install with:
  python setup.py install

aws-cmd is a runnable python module.

Run it with  after install:
    python -m aws-cmd
or
    pyton aws-cmd in this directory
or
    directly: aws-cmd/aws_cmd.py

Module is also on PyPI so you can install this package directly from PyPI using 

    pip install aws-cmd
