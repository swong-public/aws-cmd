
all:
		@echo targets: clean dist upload
		@echo "   clean - removes dist director"
		@echo "   dist - build and package into dist directory"
		@echo "   upload - upload built package in dist to PyPI"

dist:
		python setup.py sdist

# upload to PyPI
upload:
		twine upload dist/*

clean:
	rm -rf dist MANIFEST


